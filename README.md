# kuber 0.1

> Настроить kubectl со своей машинки

Для использования контекста я скопировал содержимое файла admin.conf с мастера в файл ~/.kube/config

> Command to deploy both services and deployment:
- `kubectl apply -f deploy.yml -f svc.yml`


> How to use services:
ClusterIP:
`kubectl port-forward svc/my-clusterip 8080:80`

NodePort:
Check your node ip within port you set up with `nodePort:`

> _Описать своими словами что такое readiness/liveness пробы и в чем различие между ними(их поведением)._

**livenessProbe** — Используется для проверки "живости" приложения, чтобы понять, когда процесс в контейнере завис или вошел в состояние deadlock и под с приложением стоит пересоздать.

**readinessProbe** — Проверяет, готово ли приложение к обработке запросов, и не направляет трафик до тех пор, пока проверка для всех подов не завершится успехом.
